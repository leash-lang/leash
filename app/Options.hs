module Options
  ( parse
  , General(General, debug)
  , Mode(Execute, StdinOrRepl, Typecheck)
  , Options(Options)
  ) where

import           Options.Applicative

newtype General = General
  { debug :: Bool
  }

data Mode = Typecheck FilePath
          | Execute FilePath
          | StdinOrRepl

data Options = Options General Mode

typecheck :: Parser Mode
typecheck = Typecheck <$> strOption
  (long "typecheck" <> short 't' <> metavar "FILE" <> help helptext)
  where helptext = "a script to typecheck"

execute :: Parser Mode
execute = Execute <$> argument str (metavar "FILE" <> help helptext)
  where helptext = "a script to run"

stdinOrRepl :: Parser Mode
stdinOrRepl = pure StdinOrRepl

mode :: Parser Mode
mode = typecheck <|> execute <|> stdinOrRepl

debugFlag :: Parser Bool
debugFlag = switch (long "debug" <> short 'd' <> help helptext)
  where helptext = "be talkative"

general :: Parser General
general = General <$> debugFlag

options :: Parser Options
options = Options <$> general <*> mode

parse :: IO Options
parse = execParser $ info options $ header desc where desc = "leash"
