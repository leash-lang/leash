{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NamedFieldPuns #-}
module Main where

import           Control.Monad.Except
import           Data.Functor                   ( (<&>) )
import qualified Data.Text                     as T
import           Data.Text                      ( unpack )
import           Language.Leash.Interpreter     ( InterpreterError
                                                , Value(..)
                                                )
import qualified Language.Leash.Interpreter    as Interpreter
import           Language.Leash.Type            ( Sigma )
import           Options
import           Prelude                 hiding ( print
                                                , read
                                                , unwords
                                                )
import           Prettyprinter
import           System.Console.Haskeline
import           System.Environment.XDG.BaseDir ( getUserCacheDir )
import           System.Exit                    ( ExitCode(..)
                                                , exitWith
                                                )
import           System.IO                      ( hPutStrLn
                                                , stderr
                                                )
import           System.Posix.IO                ( stdInput )
import           System.Posix.Terminal          ( queryTerminal )
import           System.Process                 ( callCommand )
----------------------------------------------------------------------------

data EvalControl
    = EvalInterpret String
    | EvalTypecheck String
    | EvalNop

data PrintControl
    = PrintNop
    | PrintError InterpreterError
    | PrintResult Value
    | PrintType Sigma

replRead :: InputT IO EvalControl
replRead = getInputLine "λ " >>= \case
  Nothing                -> return EvalNop
  Just (':' : 't' : str) -> return $ EvalTypecheck str
  Just str               -> return $ EvalInterpret str

toCmd :: [Value] -> T.Text
toCmd = toCmd' ""
 where
  toCmd' :: T.Text -> [Value] -> T.Text
  toCmd' acc (VString x : xs) = toCmd' (acc <> " " <> x) xs
  toCmd' acc []               = acc
  toCmd' _   _                = "error"

runEff :: [Value] -> IO ()
runEff vs = callCommand . unpack $ toCmd vs

replEval :: EvalControl -> IO PrintControl
replEval = \case
  EvalNop           -> return PrintNop
  EvalInterpret str -> liftIO $ Interpreter.eval str >>= \case
    Left  err -> return . PrintError $ err
    Right res -> return . PrintResult $ res
  EvalTypecheck str -> case Interpreter.typecheck str of
    Left  err -> return . PrintError $ err
    Right res -> return . PrintType $ res

replPrint :: PrintControl -> InputT IO ()
replPrint = \case
  PrintNop        -> return ()
  PrintError  err -> outputStrLn $ "Error: " <> show (pretty err)
  PrintResult res -> outputStrLn . show . pretty $ res
  PrintType   tau -> outputStrLn . show . pretty $ tau

printNonInteractive :: Bool -> PrintControl -> IO ExitCode
printNonInteractive debug = \case
  PrintNop -> return ExitSuccess
  PrintError err ->
    hPutStrLn stderr ("Error: " <> show (pretty err)) >> return (ExitFailure 1)
  PrintResult r -> print (show . pretty $ r) >> return ExitSuccess
  PrintType   t -> print (show . pretty $ t) >> return ExitSuccess
  where print = if debug then hPutStrLn stderr else return (return ())

haskelineSettings :: IO (Settings IO)
haskelineSettings = do
  historyFile <- getUserCacheDir "leash.history" <&> Just
  return $ defaultSettings { historyFile }

repl :: General -> IO ()
repl _ = haskelineSettings >>= flip runInputT go
  where go = replRead >>= (liftIO . replEval) >>= replPrint >> go

execStdin :: General -> IO ()
execStdin (General debug) = go >>= exitWith
 where
  go   = read >>= replEval >>= printNonInteractive debug
  read = getContents <&> EvalInterpret

main :: IO ()
main = parse >>= \case
  (Options general StdinOrRepl) -> queryTerminal stdInput >>= \case
    True  -> repl general
    False -> execStdin general

  (Options (General debug) (Execute file)) -> go >>= exitWith
   where
    go   = read >>= replEval >>= printNonInteractive debug
    read = readFile file <&> EvalInterpret

  (Options (General debug) (Typecheck file)) -> go >>= exitWith
   where
    go   = read >>= replEval >>= printNonInteractive debug
    read = readFile file <&> EvalTypecheck
