local configs = require 'lspconfig/configs'
local util = require 'lspconfig/util'

configs.leash_lsp_server ={
  default_config = {
    cmd = {"leash-lsp-server"};
    filetypes = {"leash"};
    root_dir = util.root_pattern(".git", vim.fn.getcwd());
    docs = {
      description = [[
https://gitlab.com/leash-lang/leash/tree/master/lsp-server
language server for leash
]];
      default_config = {
        root_dir = [[root_pattern(".git", vim.fn.getcwd())]];
      };
    }
  }
}
