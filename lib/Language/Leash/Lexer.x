{

{-# LANGUAGE OverloadedStrings                  #-}
{-# LANGUAGE NoMonomorphismRestriction          #-}
{-# LANGUAGE CPP                                #-}
{-# OPTIONS_GHC -fno-warn-unused-binds          #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures    #-}
{-# OPTIONS_GHC -fno-warn-unused-matches        #-}
{-# OPTIONS_GHC -fno-warn-unused-imports        #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing        #-}
{-# OPTIONS_GHC -fno-warn-tabs                  #-}
{-# OPTIONS_GHC -funbox-strict-fields           #-}

module Language.Leash.Lexer
  ( Alex(..)
  , AlexPosn(..)
  , AlexState(..)
  , Token(..)
  , TokenClass(..)
  , alexError
  , alexGetPosition
  , alexGetUserState
  , alexShowError
  , alexLexeme
  , runAlex
  )
where

import qualified Data.Text as T
import           System.Exit
}

%wrapper "monadUserState"

$digit = 0-9                    -- digits
$alpha = [a-zA-Z]               -- alphabetic characters

tokens :-
  <0> $white+                               ;
  <0> "#".*                                 ;
  <0> let                                   { alexPushToken $ const TokenLet        }
  <0> in                                    { alexPushToken $ const TokenIn         }
  <0> if                                    { alexPushToken $ const TokenIf         }
  <0> of                                    { alexPushToken $ const TokenOf         }
  <0> then                                  { alexPushToken $ const TokenThen       }
  <0> else                                  { alexPushToken $ const TokenElse       }
  <0> true                                  { alexPushToken $ const TokenTrue       }
  <0> false                                 { alexPushToken $ const TokenFalse      }
  <0> fn                                    { alexPushToken $ const TokenLambda     }
  <0> fix                                   { alexPushToken $ const TokenFix        }
  <0> default                               { alexPushToken $ const TokenDefault    }
  <0> "=="                                  { alexPushToken $ TokenBuiltin . T.pack   }
  <0> "<>"                                  { alexPushToken $ TokenBuiltin . T.pack   }
  <0> "++"                                  { alexPushToken $ TokenBuiltin . T.pack   }
  <0> "+"                                   { alexPushToken $ TokenBuiltin . T.pack   }
  <0> "-"                                   { alexPushToken $ TokenBuiltin . T.pack   }
  <0> "*"                                   { alexPushToken $ TokenBuiltin . T.pack   }
  <0> $digit+                               { alexPushToken $ TokenInt . read       }
  <0> [=]                                   { alexPushToken $ const TokenEq         }
  <0> [\(]                                  { alexPushToken $ const TokenLParen     }
  <0> [\)]                                  { alexPushToken $ const TokenRParen     }
  <0> [\{]                                  { alexPushToken $ const TokenLCurly     }
  <0> [\}]                                  { alexPushToken $ const TokenRCurly     }
  <0> [\[]                                  { alexPushToken $ const TokenLBracket   }
  <0> [\]]                                  { alexPushToken $ const TokenRBracket   }
  <0> ","                                   { alexPushToken $ const TokenComma      }
  <0> "=>"                                  { alexPushToken $ const TokenAbsArr     }
  <0> "|"                                   { alexPushToken $ const TokenPipe       }
  <0> "."                                   { alexPushToken $ const TokenPeriod     }
  <0> ";"                                   { alexPushToken $ const TokenSemi       }
  <0> "::"                                  { alexPushToken $ const TokenTypeAnn    }
  <0> "->"                                  { alexPushToken $ const TokenArrow      }
  <0> "Int"                                 { alexPushToken $ const TokenIntType    }
  <0> "String"                              { alexPushToken $ const TokenStringType }
  <0> "Bool"                                { alexPushToken $ const TokenBoolType   }
  <0> ":" [$alpha $digit \_ \']*            { alexPushToken $ TokenAtom . T.pack    }
  <0> $alpha [$alpha $digit \_ \']*         { alexPushToken $ TokenVar . T.pack     }
  <0> \"                                    { begin stringSC }
  <stringSC> \"                             { alexBuildString `andBegin` 0 }
  <stringSC> .                              { alexAppendChar }

{

alexBuildString :: AlexAction Token
alexBuildString (posn, _, _, s) len = do
  st <- alexGetUserState
  alexSetUserState AlexUserState { stringBuffer = "" }
  return $ Token posn $ TokenString (stringBuffer st)

alexAppendChar :: AlexAction Token
alexAppendChar input@(posn, _, _, s) len = do
  st <- alexGetUserState
  alexSetUserState st { stringBuffer = (stringBuffer st) <> (T.pack $ take len s) }
  skip input len

alexPushToken :: (String -> TokenClass) -> AlexAction Token
alexPushToken tokenizer (posn, _, _, s) len = return $ Token posn (tokenizer $ take len s)

alexEOF :: Alex Token
alexEOF = alexGetInput >>= \(p, _, _, _) -> return $ Token p TokenEOF

alexInitUserState :: AlexUserState
alexInitUserState = AlexUserState { stringBuffer = "" }

alexGetPosition :: Alex AlexPosn
alexGetPosition = Alex $ \s@AlexState { alex_pos = pos } -> Right (s, pos)

alexShowError :: (Show t1, Show t2) => (t1, t2, Maybe String) -> Alex a
alexShowError (line, column, e) = alexError $ "show-error: " ++ (show (line, column, e))

alexLexeme :: (Token -> Alex a) -> Alex a
alexLexeme f = alexMonadScan >>= f

data AlexUserState = AlexUserState { stringBuffer :: T.Text }

data Token
  = Token AlexPosn TokenClass
    deriving (Show)

data TokenClass
 = TokenAbsArr
 | TokenBuiltin T.Text
 | TokenComma
 | TokenDefault
 | TokenEOF
 | TokenEffect
 | TokenElse
 | TokenEq
 | TokenFalse
 | TokenFix
 | TokenIf
 | TokenIn
 | TokenInt    Int
 | TokenLBracket
 | TokenLCurly
 | TokenLParen
 | TokenLambda
 | TokenLess
 | TokenLessEqual
 | TokenLet
 | TokenOf
 | TokenPeriod
 | TokenPipe
 | TokenRBracket
 | TokenRCurly
 | TokenRParen
 | TokenSemi
 | TokenString T.Text
 | TokenThen
 | TokenTrue
 | TokenVar    T.Text
 | TokenAtom T.Text
 | TokenArrow
 | TokenTypeAnn
 | TokenIntType
 | TokenStringType
 | TokenBoolType
   deriving (Eq, Show)
}
