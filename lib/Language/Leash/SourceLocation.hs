module Language.Leash.SourceLocation
  ( SourceLocation(..)
  ) where

import           Data.Data
import           Prettyprinter

data SourceLocation = SourceLocation
  { line   :: !Int
  , column :: !Int
  }
  deriving (Eq, Ord, Show, Typeable, Data)

instance Pretty SourceLocation where
  pretty (SourceLocation l c) =
    concatWith (surround comma) [pretty l, pretty c]
