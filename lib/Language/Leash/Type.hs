{-# LANGUAGE ScopedTypeVariables #-}
module Language.Leash.Type
  ( Tau(..)
  , Sigma(..)
  , Context
  , Theta
  , (<@>)
  , Types(..)
  , Substitution(..)
  , Text
  , generalize
  ) where

--------------------------------------------------------------------------------

import           Data.Data
import           Data.Foldable                  ( fold )
import           Data.Functor.Foldable          ( Base
                                                , cata
                                                , embed
                                                )
import           Data.Functor.Foldable.TH
import qualified Data.List.NonEmpty            as L
import           Data.List.NonEmpty             ( (<|) )
import           Data.Map                       ( Map )
import qualified Data.Map                      as Map
import           Data.Map.Extra                 ( deleteList )
import           Data.Set                       ( Set )
import qualified Data.Set                      as Set
import           Data.Text                      ( Text
                                                , singleton
                                                )
import           Prettyprinter           hiding ( annotate )
-- import           Prettyprinter.Extra            ( braced )

--------------------------------------------------------------------------------

data Tau
    = TVar !Text
    | TArrow !Tau !Tau
    | TEmptyRow
    | TExtendRow !(Map Text (L.NonEmpty Tau)) !Tau
    | TRecord !Tau
    | TInt
    | TBool
    | TString
    | TAtom !Text
    | TTuple !Tau !(L.NonEmpty Tau)
    | TList !Tau
    deriving (Eq, Ord, Show, Typeable, Data)

data Sigma
  = TForAll ![Text] !Tau
  | TMono !Tau
  deriving (Eq, Ord, Show, Typeable, Data)

makeBaseFunctor ''Tau
makeBaseFunctor ''Sigma

--------------------------------------------------------------------------------

type Context = Map Text Sigma

--------------------------------------------------------------------------------

type Theta = Map Text Tau

class Substitution a where
  apply :: Theta -> a -> a

instance Substitution Context where
  apply s = Map.map (apply s)

instance Substitution Tau where
  apply s = cata go
   where
    go (TVarF x) = case Map.lookup x s of
      Nothing -> TVar x
      Just t  -> apply s t
    go t = embed t

instance Substitution Sigma where
  apply s = cata go
   where
    go (TMonoF t     ) = TMono $ apply s t
    go (TForAllF xs t) = TForAll xs $ apply (deleteList xs s) t


infixl 9 <@>
(<@>) :: Theta -> Theta -> Theta
s1 <@> s2 = Map.map (apply s1) s2 `Map.union` s1

--------------------------------------------------------------------------------

generalize :: Context -> Tau -> Sigma
generalize ctx t = TForAll vs' (apply sub t)
 where
  cs  = Set.unions $ map tyVars $ Map.elems ctx
  vs  = Set.toList $ tyVars t `Set.difference` cs
  vs' = snd <$> zip vs (singleton <$> ['a' ..])
  sub = Map.fromList $ zip vs $ TVar <$> vs'

--------------------------------------------------------------------------------

class Types a where
    tyVars :: a -> Set Text

instance Types Context where
  tyVars ctx = tyVars (Map.elems ctx)

instance Types a => Types [a] where
  tyVars = foldMap tyVars

instance Types Tau where
  tyVars = cata alg   where
    alg :: Base Tau (Set Text) -> Set Text
    alg (TVarF i) = Set.singleton i
    alg e         = fold e

instance Types Sigma where
  tyVars = cata alg   where
    alg :: Base Sigma (Set Text) -> Set Text
    alg (TForAllF is t) = tyVars t `Set.difference` Set.fromList is
    alg (TMonoF t     ) = tyVars t

--------------------------------------------------------------------------------

mapPair :: (a -> b, c -> d) -> (a, c) -> (b, d)
mapPair (f, f') (t, t') = (f t, f' t')

ppTuple :: (Pretty a) => Doc b -> (a, Doc b) -> Doc b
ppTuple sep1 x = uncurry (surround sep1) (mapPair (pretty, id) x)

ppMap :: (Pretty a) => Doc b -> Doc b -> Map a (Doc b) -> Doc b
ppMap sep0 sep1 s = concatWith (surround sep0) (ppTuple sep1 <$> Map.toList s)

xfrm :: Map Text (L.NonEmpty (b, Doc a)) -> Map Text (L.NonEmpty (Doc a))
xfrm m = fmap snd <$> m

ppLabels :: Map Text (L.NonEmpty (Doc a)) -> Doc a
ppLabels = ppMap ", " " :: " . fmap L.head

instance Pretty Sigma where
  pretty = cata go
   where
    go (TForAllF [] t) = pretty t
    go (TForAllF vs t) = "forall" <+> sep (fmap pretty vs) <> "." <+> pretty t
    go (TMonoF t     ) = pretty t


newtype PrettyContext = PrettyContext
  { _isArrow :: Bool
  }

arr :: PrettyContext
arr = PrettyContext { _isArrow = True }

noArr :: PrettyContext
noArr = PrettyContext { _isArrow = False }

isArrow :: (PrettyContext, a) -> Bool
isArrow = _isArrow . fst

parensIf :: ((PrettyContext, Doc a) -> Bool) -> (PrettyContext, Doc a) -> Doc a
parensIf predi term = (if predi term then parens else id) $ snd term

instance Pretty Tau where
  pretty = snd . cata alg
   where
    alg :: Base Tau (PrettyContext, Doc a) -> (PrettyContext, Doc a)
    alg (TVarF x    )      = (noArr, pretty x)
    alg (TArrowF a b)      = (arr, surround " -> " (parensIf isArrow a) (snd b))
    alg TEmptyRowF         = (noArr, braces "")
    alg (TExtendRowF ls r) = (noArr, hsep [ppLabels . xfrm $ ls, "|", snd r])
    alg (TRecordF r      ) = (noArr, braces $ snd r)
    alg (TAtomF   x      ) = (noArr, pretty x)
    alg TIntF              = (noArr, "Int")
    alg TBoolF             = (noArr, "Bool")
    alg TStringF           = (noArr, "String")
    alg (TTupleF x xs)     = (noArr, tupled . L.toList $ snd <$> x <| xs)
    alg (TListF t    )     = (noArr, brackets $ snd t)

--------------------------------------------------------------------------------
