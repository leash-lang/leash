{-# LANGUAGE ScopedTypeVariables #-}

{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}

module Language.Leash.Interpreter where

import           Control.Monad.Except
import           Control.Monad.State            ( MonadState
                                                , StateT
                                                , runStateT
                                                )
import           Data.Function                  ( (&) )
import           Data.Functor                   ( (<&>) )
import           Data.Functor.Foldable          ( Base
                                                , embed
                                                , para
                                                )
import           Data.IORef                     ( IORef
                                                , newIORef
                                                , readIORef
                                                , writeIORef
                                                )
import           Data.List.NonEmpty             ( (<|)
                                                , NonEmpty
                                                )
import qualified Data.List.NonEmpty            as L
import           Data.Map                       ( Map )
import qualified Data.Map                      as Map
import qualified Data.Text                     as T
import           Language.Leash.Annotated
import           Language.Leash.Ast
import           Language.Leash.Cst             ( Cst'
                                                , toCst
                                                )
import qualified Language.Leash.Inference      as I
                                                ( InferenceError
                                                , typecheck
                                                )
import           Language.Leash.Parser
import           Language.Leash.Rewrite         ( rewrite )
import           Language.Leash.SourceLocation  ( SourceLocation(..) )
import           Language.Leash.Type
import           Prettyprinter           hiding ( annotate )
import           Prettyprinter.Extra            ( braced
                                                , label
                                                )

--------------------------------------------------------------------------------

type Thunk = () -> Interpreter Value

data Value
  = VInt Int
  | VBool Bool
  | VBuiltin T.Text
  | VString !T.Text
  | VRecord (Map T.Text Value)
  | VAtom !T.Text
  | VTuple Value !(NonEmpty Value)
  | VClosure (Thunk -> Interpreter Value)
  | VList ![Value]

newtype Interpreter a = Interpreter { _runInterpreter :: StateT Env (ExceptT InterpreterError IO) a }
  deriving (Applicative, Functor, Monad, MonadIO, MonadError InterpreterError, MonadState Env)

runInterpreter :: Interpreter Value -> IO (Either InterpreterError Value)
runInterpreter m = runExceptT $ runStateT (_runInterpreter m) Map.empty <&> fst

--------------------------------------------------------------------------------

data InterpreterError
  = EParseError ParseError
  | EInferenceError I.InferenceError
  | EUnboundVariableError T.Text SourceLocation
  | ENotARecordError T.Text
  | ENotARecordValueError Value
  | ENotAClosureError Value
  | ELabelMissingError T.Text
  | EUnknownBuiltinError T.Text
  | ENotABoolError Value
  | ENotAnAtomError Value
  | EFatalError T.Text

unboundVariableError :: T.Text -> SourceLocation -> Interpreter a
unboundVariableError x loc = throwError $ EUnboundVariableError x loc

notARecordError :: T.Text -> Interpreter a
notARecordError x = throwError $ ENotARecordError x

notARecordValueError :: Value -> Interpreter a
notARecordValueError x = throwError $ ENotARecordValueError x

notAnAtomValueError :: Value -> Interpreter a
notAnAtomValueError x = throwError $ ENotAnAtomError x

labelMissingError :: T.Text -> Interpreter a
labelMissingError x = throwError $ ELabelMissingError x

notAClosureError :: Value -> Interpreter a
notAClosureError = throwError . ENotAClosureError

unknownBuiltinError :: T.Text -> Interpreter a
unknownBuiltinError = throwError . EUnknownBuiltinError

notABoolError :: Value -> Interpreter a
notABoolError = throwError . ENotABoolError

fatalError :: T.Text -> Interpreter a
fatalError = throwError . EFatalError

--------------------------------------------------------------------------------

lookupRow :: T.Text -> Value -> Interpreter Value
lookupRow x (VRecord r) = case Map.lookup x r of
  Just v  -> return v
  Nothing -> labelMissingError x
lookupRow x _ = notARecordError x

--------------------------------------------------------------------------------

alg :: Base Ast' (Ast', Env -> Interpreter Value) -> Env -> Interpreter Value

alg (TypedF x _ :*: K _  ) env = para alg (fst x) env

alg (VarF x     :*: K loc) env = case Map.lookup x env of
  Just v  -> force v
  Nothing -> unboundVariableError x loc

alg (AtomF x      :*: K _) _   = return $ VAtom x

alg (AbsF x e     :*: K _) env = return . VClosure $ mkThunk env x (fst e)

alg (LetF x e0 e1 :*: K _) env = mkThunk env x (fst e1) (const $ snd e0 env)

alg (BuiltinF b   :*: K _) env = case Map.lookup b env of
  Just ref -> force ref
  Nothing  -> unknownBuiltinError b

alg (IntF    i  :*: K _) _   = return $ VInt i

alg (BoolF   v  :*: K _) _   = return $ VBool v

alg (StringF v  :*: K _) _   = return $ VString v

alg (AppF e0 e1 :*: K _) env = snd e0 env >>= \case
  (VClosure cls) -> cls . const $ snd e1 env
  x              -> notAClosureError x

-- this annoyingly recurses into the algebra again, it's pretty ugly
alg (FixF e :*: K loc) env = para alg term env
 where
  term = mkApp' (fst e) $ mkFix' (fst e)
  mkApp' e0' e1' = embed $ AppF e0' e1' :*: K loc
  mkFix' e' = embed $ FixF e' :*: K loc

alg (IfF e0 e1 e2 :*: K _) env = snd e0 env >>= \case
  VBool True  -> snd e1 env
  VBool False -> snd e2 env
  x           -> notABoolError x

alg (EmptyRecordF           :*: K _) _   = return $ VRecord Map.empty

alg (ExtendRecordF ext rest :*: K _) env = do
  ext'  <- sequence $ Map.map (`snd` env) (Map.map L.head ext)
  rest' <- snd rest env
  case rest' of
    VRecord xs -> return . VRecord $ Map.union ext' xs
    x          -> notARecordValueError x

alg (SelectRecordF x e :*: K loc) env = snd e env >>= \case
  VRecord rs -> case Map.lookup x rs of
    Just v  -> return v
    Nothing -> unboundVariableError x loc
  v -> notARecordValueError v

-- alg (VariantF x e  :*: K _) env = snd e env <&> VVariant x

alg (TupleF e es :*: K _) env = do
  v  <- snd e env
  vs <- ((env &) . snd) `mapM` es
  return $ VTuple v vs

-- alg (CaseF e cs :*: _) env = snd e env >>= \case
--   (VVariant el ev) ->
--     let go' = \case
--           (VariantCase vl vx ve) | vl == el ->
--             [mkThunk env vx (fst ve) (const . return $ ev)]
--           (DefaultCase vx ve) ->
--             [mkThunk env vx (fst ve) (const . return $ ev)]
--           _ -> []
--     in  head $ foldMap go' cs
--   z -> notAVariantValueError z

alg (ListF xs :*: _) env = mapM (`snd` env) xs <&> VList

--------------------------------------------------------------------------------
-- | TODO: This shit is ugly AF, come up with a decent abstraction

type Env = Map T.Text (IORef Thunk)

force :: IORef Thunk -> Interpreter Value
force ref = do
  th <- liftIO $ readIORef ref
  v  <- th ()
  update ref v
  return v

update :: IORef Thunk -> Value -> Interpreter ()
update ref v = do
  liftIO $ writeIORef ref (\() -> return v)
  return ()

mkThunk :: Env -> T.Text -> Ast' -> Thunk -> Interpreter Value
mkThunk env x term th = do
  th' <- liftIO $ newIORef th
  -- TODO: Calling back into the tree is ugly, reuse the env so
  --       we can do a cata rather than para
  para alg term (Map.insert x th' env)

--------------------------------------------------------------------------------

eq :: Value -> Value -> Interpreter Value
eq (VInt  a) (VInt  b) = return . VBool $ a == b
eq (VBool a) (VBool b) = return . VBool $ a == b
eq _         _         = fatalError "cannot compare"

add :: Value -> Value -> Interpreter Value
add (VInt a) (VInt b) = return . VInt $ a + b
add _        _        = fatalError "cannot add"

mul :: Value -> Value -> Interpreter Value
mul (VInt a) (VInt b) = return . VInt $ a * b
mul _        _        = fatalError "cannot mul"

sub :: Value -> Value -> Interpreter Value
sub (VInt a) (VInt b) = return . VInt $ a - b
sub _        _        = fatalError "cannot sub"

append :: Value -> Value -> Interpreter Value
append (VString a) (VString b) = return . VString $ a <> b
append _           _           = fatalError "cannot append"

listConcat :: Value -> Value -> Interpreter Value
listConcat (VList a) (VList b) = return . VList $ a ++ b
listConcat _         _         = fatalError "cannot concat"

-- TODO: oh god, this is ugly and I don't understand it myself even
mkBuiltin
  :: T.Text
  -> (Value -> Value -> Interpreter Value)
  -> Interpreter (T.Text, IORef Thunk)
mkBuiltin name op = do
  ref <- liftIO $ newIORef $ \() -> return . VClosure $ \thl ->
    return . VClosure $ \thr -> do
      a <- thl ()
      b <- thr ()
      op a b
  return (name, ref)

initialContext :: Context
initialContext = Map.fromList
  [ ("+" , TMono $ int `TArrow` (int `TArrow` int))
  , ("-" , TMono $ int `TArrow` (int `TArrow` int))
  , ("*" , TMono $ int `TArrow` (int `TArrow` int))
  , ("==", TForAll ["a"] $ a `TArrow` (a `TArrow` bool))
  , ("<>", TMono $ string `TArrow` (string `TArrow` string))
  , ("++", TForAll ["a"] $ TList a `TArrow` (TList a `TArrow` TList a))
  ]
 where
  a      = TVar "a"
  int    = TInt
  bool   = TBool
  string = TString

mkInitialEnv :: Interpreter Env
mkInitialEnv = sequence ops <&> Map.fromList
 where
  ops =
    [ mkBuiltin "+"  add
    , mkBuiltin "-"  sub
    , mkBuiltin "*"  mul
    , mkBuiltin "==" eq
    , mkBuiltin "<>" append
    , mkBuiltin "++" listConcat
    ]

--------------------------------------------------------------------------------

mapLeft :: (a -> c) -> Either a b -> Either c b
mapLeft f = either (Left . f) Right

eval :: String -> IO (Either InterpreterError Value)
eval input = runInterpreter go
 where
  go :: Interpreter Value
  go = do
    initialEnv <- mkInitialEnv
    terms      <- liftEither (typecheck input) >> parse' input <&> rewrite
    infer' terms >> para alg terms initialEnv

  -- | wrapper to run the parser in our Interpreter monad
  parse' :: String -> Interpreter Cst'
  parse' i = liftEither $ mapLeft EParseError (parse i) <&> toCst

  -- | wrapper to run the type inference in our Interpreter monad
  infer' :: Ast' -> Interpreter (Context, Tau)
  infer' terms =
    liftEither $ mapLeft EInferenceError (I.typecheck initialContext terms)

typecheck :: String -> Either InterpreterError Sigma
typecheck s = do
  -- TODO: double parsing and desugaring happens because eval calls typecheck
  term     <- liftEither $ mapLeft EParseError (parse s) <&> rewrite . toCst
  (_, tau) <- liftEither
    $ mapLeft EInferenceError (I.typecheck initialContext term)
  return $ generalize initialContext tau

--------------------------------------------------------------------------------

-- TODO: use a cata here
instance Pretty InterpreterError where
  pretty = \case
    (EParseError     e) -> "parse" <+> pretty e
    (EInferenceError e) -> "inference" <+> pretty e
    (EUnboundVariableError e loc) ->
      "unbound variable:" <+> pretty e <+> pretty loc
    (ELabelMissingError    x  ) -> "label missing:" <+> pretty x
    (ENotARecordError      x  ) -> "not a record:" <+> pretty x
    (ENotARecordValueError x  ) -> "not a record value:" <+> pretty x
    (ENotAnAtomError       x  ) -> "not an atom:" <+> pretty x
    (ENotAClosureError     x  ) -> "not a closure:" <+> pretty x
    (EUnknownBuiltinError  op ) -> "unknown builtin:" <+> pretty op
    (EFatalError           txt) -> "fatal:" <+> pretty txt
    (ENotABoolError        v  ) -> "not a bool:" <+> pretty v


-- TODO: use a cata here
instance Pretty Value where
  pretty = \case
    (VInt     i    ) -> pretty i
    (VBool    True ) -> "true"
    (VBool    False) -> "false"
    (VString  s    ) -> dquotes $ pretty s
    (VRecord  ls   ) -> braced . Map.elems $ Map.mapWithKey label ls
    (VClosure _    ) -> "<<closure>>"
    (VBuiltin b    ) -> pretty b
    (VAtom    x    ) -> pretty x
    (VTuple x xs   ) -> tupled $ pretty <$> L.toList (x <| xs)
    (VList xs      ) -> list $ pretty <$> xs
