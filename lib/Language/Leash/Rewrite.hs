module Language.Leash.Rewrite
  ( rewrite
  ) where

import           Data.Functor.Foldable          ( Base
                                                , cata
                                                , embed
                                                )
import           Data.List.NonEmpty             ( (<|)
                                                , NonEmpty(..)
                                                )
import qualified Data.List.NonEmpty            as L
                                                ( head
                                                , reverse
                                                , zip
                                                )
import           Language.Leash.Annotated
import qualified Language.Leash.Ast            as Ast
import           Language.Leash.Ast             ( Ast' )
import           Language.Leash.Cst

expandMultiArgs :: Cst' -> Cst'
expandMultiArgs = cata alg
 where
  -- |
  -- | the expression
  -- |   let f a b = a + b
  -- | expands to
  -- |   let f = fix (fn f => fn a => fn b => a + b
  -- |
  alg (LetRecF n bs e0 e1 :*: K loc) = mkLet'
    (VanillaArg n)
    (mkFix' $ foldr mkAbs' e0 binders)
    e1
   where
    binders = VanillaArg n <| bs
    mkAbs' b' e' = mkAbs (b' :| []) e' loc
    mkLet' b' e0' e1' = mkLet b' e0' e1' loc
    mkFix' e' = mkFix e' loc
  -- |
  -- | the expression
  -- |   fn a b => a + b
  -- | expands to
  -- |   fn a => fn b => a + b
  alg (AbsF bs e :*: K loc) = foldr mkAbs' e bs
    where mkAbs' b' e' = mkAbs (b' :| []) e' loc
  -- |
  -- | the rest is left untouched
  -- |
  alg x = embed x

expandPatterns :: Cst' -> Cst'
expandPatterns = cata alg
 where
  -- | the expression
  -- |   fn { a, b } => a + b
  -- | expands to
  -- |   fn k => (fn a => fn b => a + b) (k.a) (k.b)
  -- |   ^outer  ^inner                  ^args
  alg (AbsF (DeconstructArg bs :| []) e :*: K loc) = do
    let inner = foldr mkAbs' e bs
        args  = select' (mkVar' "k") <$> bs
    mkAbs' "k" $ foldr mkApp' inner (L.reverse args)
   where
    mkAbs' b' e' = mkAbs (VanillaArg b' :| []) e' loc
    mkVar' x' = mkVar x' loc
    mkApp' e1' e0' = mkApp e0' e1' loc
    select' e' l' = mkSelectRecord l' e' loc
  -- | the expression
  -- |   let { a, b } = e0 in e1
  -- | expands to
  -- |   let a = e0.a in let b = e0.b in e1
  alg (LetF (DeconstructArg bs) e0 e1 :*: K loc) = do
    let lets = mkLet' <$> L.zip bs (select' e0 <$> bs)
    compose' lets e1
   where
    compose' = foldl (flip (.)) id
    mkLet' (b', e0') e1' = mkLet (VanillaArg b') e0' e1' loc
    select' e' l' = mkSelectRecord l' e' loc
  -- |
  -- | the rest is left untouched
  -- |
  alg x = embed x

rewrite :: Cst' -> Ast'
rewrite = cata alg . expandPatterns . expandMultiArgs
 where
  alg :: Base Cst' Ast' -> Ast'

  alg (VarF x     :*: loc) = embed $ Ast.VarF x :*: loc

  alg (AppF e0 e1 :*: loc) = embed $ Ast.AppF e0 e1 :*: loc

  -- We assume that the rewrites have eliminated both multi
  -- argument abstractions and abstractions containing something
  -- else than VanillaArg's
  alg (AbsF bs e  :*: loc) = embed $ Ast.AbsF bs' e :*: loc
   where
    bs' = case L.head bs of
      (VanillaArg     x) -> x
      (DeconstructArg _) -> error "unexpected deconstruct arg"

  alg (LetF (VanillaArg b) e0 e1 :*: loc) = embed $ Ast.LetF b e0 e1 :*: loc
  alg (LetF{}                    :*: _  ) = error "unexpected deconstruct arg"
  alg (LetRecF{}                 :*: _  ) = error "unexpected LetRec"
  alg (BuiltinF b                :*: loc) = embed $ Ast.BuiltinF b :*: loc
  alg (IfF p e0 e1               :*: loc) = embed $ Ast.IfF p e0 e1 :*: loc
  alg (FixF    e                 :*: loc) = embed $ Ast.FixF e :*: loc
  alg (IntF    i                 :*: loc) = embed $ Ast.IntF i :*: loc
  alg (BoolF   i                 :*: loc) = embed $ Ast.BoolF i :*: loc
  alg (StringF str               :*: loc) = embed $ Ast.StringF str :*: loc
  alg (EmptyRecordF              :*: loc) = embed $ Ast.EmptyRecordF :*: loc
  alg (ExtendRecordF rs r :*: loc) = embed $ Ast.ExtendRecordF rs r :*: loc
  alg (SelectRecordF l r :*: loc) = embed $ Ast.SelectRecordF l r :*: loc
  alg (TupleF        x  xs       :*: loc) = embed $ Ast.TupleF x xs :*: loc
  alg (AtomF x                   :*: loc) = embed $ Ast.AtomF x :*: loc
  alg (ListF xs                  :*: loc) = embed $ Ast.ListF xs :*: loc
  alg (TypedF x s                :*: loc) = embed $ Ast.TypedF x s :*: loc
