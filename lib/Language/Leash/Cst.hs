{-# LANGUAGE UndecidableInstances #-}

module Language.Leash.Cst
  ( Cst'
  , Cst(..)
  , CstF(..)
  , SourceLocation(..)
  , Binder(..)
  , Declaration(..)
  , Module(..)
  , mkVar
  , mkApp
  , mkAtom
  , mkAbs
  , mkBuiltin
  , mkInt
  , mkBool
  , mkString
  , mkLet
  , mkLetRec
  , mkIf
  , mkFix
  , mkEmptyRecord
  , mkExtendRecord
  , mkSelectRecord
  , mkTuple
  , mkList
  , mkTyped
  , toCst
  ) where

import qualified Data.List.NonEmpty            as L
import           Data.List.NonEmpty             ( (<|)
                                                , NonEmpty(..)
                                                )

import           Data.Data
import           Data.Fix                       ( Fix(..) )
import           Data.Functor.Foldable          ( cata )
import           Data.Functor.Foldable.TH       ( makeBaseFunctor )
import           Data.Map                       ( Map )
import qualified Data.Map                      as Map
import           Data.Text                      ( Text )
import           Language.Leash.Annotated
import           Language.Leash.SourceLocation  ( SourceLocation(..) )
import           Language.Leash.Type            ( Tau(..) )
import           Prettyprinter           hiding ( annotate )

--------------------------------------------------------------------------------

data Binder
  = VanillaArg !Text
  | DeconstructArg !(NonEmpty Text)
  deriving (Eq, Ord, Show, Typeable, Data)


data Cst
  = Var !Text
  | App !Cst !Cst
  | Abs !(NonEmpty Binder) !Cst
  | Let !Binder !Cst !Cst
  | LetRec !Text !(NonEmpty Binder) !Cst !Cst
  | Builtin !Text
  | If !Cst !Cst !Cst
  | Fix !Cst
  | Int !Int
  | Bool !Bool
  | String !Text
  | EmptyRecord
  | ExtendRecord !(Map Text (NonEmpty Cst)) !Cst
  | SelectRecord !Text !Cst
  | Tuple !Cst !(NonEmpty Cst)
  | Atom !Text
  | List ![Cst]
  | Typed !Cst Tau
  deriving (Eq, Ord, Show, Typeable, Data)

makeBaseFunctor ''Cst

type Cst' = Fix (CstF :*: K SourceLocation)

data Declaration = Declaration !Text !Cst'

data Module = Module ![Declaration] !Cst'

toCst :: Module -> Cst'
toCst (Module [] cst) = cst
toCst (Module ds cst) = foldr go cst ds
 where
  go :: Declaration -> Cst' -> Cst'
  go (Declaration name definition) next =
    mkLet (VanillaArg name) definition next (getAnn definition)

--------------------------------------------------------------------------------

mkTyped :: Cst' -> Tau -> SourceLocation -> Cst'
mkTyped x t = withLoc $ TypedF x t

mkVar :: Text -> SourceLocation -> Cst'
mkVar x = withLoc $ VarF x

mkApp :: Cst' -> Cst' -> SourceLocation -> Cst'
mkApp e0 e1 = withLoc $ AppF e0 e1

mkAbs :: NonEmpty Binder -> Cst' -> SourceLocation -> Cst'
mkAbs b e = withLoc $ AbsF b e

mkLet :: Binder -> Cst' -> Cst' -> SourceLocation -> Cst'
mkLet b e0 e1 = withLoc $ LetF b e0 e1

mkLetRec :: Text -> NonEmpty Binder -> Cst' -> Cst' -> SourceLocation -> Cst'
mkLetRec n bs e0 e1 = withLoc $ LetRecF n bs e0 e1

mkBuiltin :: Text -> SourceLocation -> Cst'
mkBuiltin b = withLoc $ BuiltinF b

mkInt :: Int -> SourceLocation -> Cst'
mkInt int = withLoc $ IntF int

mkBool :: Bool -> SourceLocation -> Cst'
mkBool bool = withLoc $ BoolF bool

mkString :: Text -> SourceLocation -> Cst'
mkString str = withLoc $ StringF str

mkIf :: Cst' -> Cst' -> Cst' -> SourceLocation -> Cst'
mkIf e0 e1 e2 = withLoc $ IfF e0 e1 e2

mkFix :: Cst' -> SourceLocation -> Cst'
mkFix e = withLoc $ FixF e

mkEmptyRecord :: SourceLocation -> Cst'
mkEmptyRecord = withLoc EmptyRecordF

mkExtendRecord :: NonEmpty (Text, Cst') -> Cst' -> SourceLocation -> Cst'
mkExtendRecord ls r = withLoc $ ExtendRecordF terms r
 where
  terms = Map.fromListWith (<>) . L.toList $ fmap liftSnd ls
  liftSnd (k, v) = (k, v :| [])

mkSelectRecord :: Text -> Cst' -> SourceLocation -> Cst'
mkSelectRecord x e = withLoc $ SelectRecordF x e

mkTuple :: Cst' -> NonEmpty Cst' -> SourceLocation -> Cst'
mkTuple x xs = withLoc $ TupleF x xs

mkList :: [Cst'] -> SourceLocation -> Cst'
mkList xs = withLoc $ ListF xs

mkAtom :: Text -> SourceLocation -> Cst'
mkAtom x = withLoc $ AtomF x

--------------------------------------------------------------------------------

withLoc :: CstF Cst' -> SourceLocation -> Cst'
withLoc = flip withAnn

--------------------------------------------------------------------------------

instance Pretty Cst' where
  pretty = cata go
   where
    go (VarF x       :*: _) = pretty x
    go (AppF e0 e1   :*: _) = e0 <+> e1
    go (AbsF p  e    :*: _) = pretty p <+> "=>" <+> e
    go (LetF p e0 e1 :*: _) = "let" <+> pretty p <+> "=" <+> e0 <+> "in" <+> e1
    go (LetRecF n ps e0 e1 :*: _) =
      "let"
        <+> pretty n
        <+> (hsep . L.toList $ pretty <$> ps)
        <+> "="
        <+> e0
        <+> "in"
        <+> e1
    go (IfF e0 e1 e2   :*: _) = "if" <+> e0 <+> "then" <+> e1 <+> "else" <+> e2
    go (FixF     e     :*: _) = "rec" <+> e
    go (IntF     i     :*: _) = pretty i
    go (BoolF    True  :*: _) = "true"
    go (BoolF    False :*: _) = "false"
    go (StringF  str   :*: _) = dquotes $ pretty str
    go (BuiltinF b     :*: _) = pretty b
    go (EmptyRecordF   :*: _) = "{}"
    go (TupleF x xs    :*: _) = tupled . L.toList $ x <| xs
    go (AtomF x        :*: _) = pretty x
    go (TypedF x t     :*: _) = x <+> "::" <+> pretty t
    go _                      = "<<unhandled>>"

instance Pretty Binder where
  pretty (VanillaArg x) = pretty x
  pretty (DeconstructArg xs) =
    braces . hsep . punctuate comma $ pretty <$> L.toList xs
