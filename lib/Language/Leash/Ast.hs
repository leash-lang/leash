module Language.Leash.Ast
  ( Ast(..)
  , AstF(..)
  , Ast'
  ) where

import           Data.Data
import           Data.Fix                       ( Fix )
import           Data.Functor.Foldable          ( cata )
import           Data.Functor.Foldable.TH       ( makeBaseFunctor )
import           Data.List.NonEmpty             ( (<|)
                                                , NonEmpty(..)
                                                )
import qualified Data.List.NonEmpty            as L
                                                ( toList )
import           Data.Map                       ( Map )
import           Data.Text                      ( Text )
import           Language.Leash.Annotated
import           Language.Leash.SourceLocation  ( SourceLocation(..) )
import           Language.Leash.Type            ( Tau(..) )
import           Prettyprinter

--------------------------------------------------------------------------------

-- |The AST is a simpler form of the CST
data Ast
  = Var !Text
  | App !Ast !Ast
  | Abs !Text !Ast
  | Let !Text !Ast !Ast
  | Builtin !Text
  | If !Ast !Ast !Ast
  | Fix !Ast
  | Int !Int
  | Bool !Bool
  | String !Text
  | EmptyRecord
  | ExtendRecord !(Map Text (NonEmpty Ast)) !Ast -- TODO: do we need a list here?
  | SelectRecord !Text !Ast
  | Tuple !Ast !(NonEmpty Ast)
  | Atom !Text
  | List ![Ast]
  | Typed !Ast !Tau
  deriving (Eq, Ord, Show, Typeable, Data)

makeBaseFunctor ''Ast

type Ast' = Fix (AstF :*: K SourceLocation)

--------------------------------------------------------------------------------
--

instance Pretty Ast' where
  pretty = cata go
   where
    go (VarF x        :*: _) = pretty x
    go (AppF e0 e1    :*: _) = e0 <+> e1
    go (AbsF p  e     :*: _) = pretty p <+> "=>" <+> e
    go (LetF p e0 e1  :*: _) = "let" <+> pretty p <+> "=" <+> e0 <+> "in" <+> e1
    go (BuiltinF b    :*: _) = pretty b
    go (IfF e0 e1 e2  :*: _) = "if" <+> e0 <+> "then" <+> e1 <+> "else" <+> e2
    go (FixF    e     :*: _) = "rec" <+> e
    go (IntF    i     :*: _) = pretty i
    go (BoolF   True  :*: _) = "true"
    go (BoolF   False :*: _) = "false"
    go (StringF str   :*: _) = dquotes $ pretty str
    go (EmptyRecordF  :*: _) = "{}"
    go (TupleF x xs   :*: _) = tupled . L.toList $ x <| xs
    go (AtomF x       :*: _) = pretty x
    go (TypedF x t    :*: _) = x <+> "::" <+> pretty t
    go _                     = "<<unhandled>>"
