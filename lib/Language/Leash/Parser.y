{
module Language.Leash.Parser where

import           Control.Monad.Trans.Error
import           Data.Functor.Foldable
import           Data.List.NonEmpty (NonEmpty ((:|)), (<|))
import qualified Data.List.NonEmpty as L
import qualified Data.Map as Map
import           Language.Leash.Annotated
import           Language.Leash.Cst
import           Language.Leash.Lexer
import           Language.Leash.Type
import           Prettyprinter

import Data.List
import Data.Fix

}

%name happyParser
%tokentype { Token }

%monad { Alex } { thenP } { returnP }
%lexer { alexLexeme } { Token _ TokenEOF }

%token
        '('             { Token _ TokenLParen       }
        ')'             { Token _ TokenRParen       }
        ','             { Token _ TokenComma        }
        '->'            { Token _ TokenArrow        }
        '.'             { Token _ TokenPeriod       }
        '::'            { Token _ TokenTypeAnn      }
        ';'             { Token _ TokenSemi         }
        '='             { Token _ TokenEq           }
        '=>'            { Token _ TokenAbsArr       }
        'Bool'          { Token _ TokenBoolType     }
        'Int'           { Token _ TokenIntType      }
        'String'        { Token _ TokenStringType   }
        '['             { Token _ TokenLBracket     }
        ']'             { Token _ TokenRBracket     }
        'default'       { Token _ TokenDefault      }
        'else'          { Token _ TokenElse         }
        'false'         { Token _ TokenFalse        }
        'fix'           { Token _ TokenFix          }
        'fn'            { Token _ TokenLambda       }
        'if'            { Token _ TokenIf           }
        'in'            { Token _ TokenIn           }
        'let'           { Token _ TokenLet          }
        'of'            { Token _ TokenOf           }
        'then'          { Token _ TokenThen         }
        'true'          { Token _ TokenTrue         }
        '{'             { Token _ TokenLCurly       }
        '|'             { Token _ TokenPipe         }
        '}'             { Token _ TokenRCurly       }
        ATOM            { Token _ (TokenAtom $$)    }
        BUILTIN         { Token _ (TokenBuiltin $$) }
        IDENT           { Token _ (TokenVar $$)     }
        INT             { Token _ (TokenInt $$)     }
        STRING          { Token _ (TokenString $$)  }

%right 'in'
%left BUILTIN
-- %left "*" "/"
-- %left ">" "<"
%nonassoc IDENT INT 'let' '(' 'fn'
%nonassoc APP 'fn'
%%

Module :: {Module}
    : 'let' Declarations 'in' Expr                 { Module $2 $4 }
    | Expr                                         { Module [] $1 }

Declarations :: {[Declaration]}
    : Declaration                                  { [$1]       }
    | Declarations Declaration                     { $2 : $1    }

Declaration :: {Declaration}
    : IDENT ArgList '=' Expr ';'                   {% annM $ \loc -> Declaration $1 (mkAbs $2 $4 loc) }

Expr :: {Cst'}
    : Expr '::' Type                               {% annM $ mkTyped $1 $3 }
    | IDENT                                        {% annM $ mkVar $1              }
    | 'let' Arg '=' Expr 'in' Expr                 {% annM $ mkLet $2 $4 $6        }
    | 'let' IDENT ArgList '=' Expr 'in' Expr       {% annM $ mkLetRec $2 $3 $5 $7  }
    | 'fn' ArgList '=>' Expr                       {% annM $ mkAbs $2 $4   }
    | Expr Expr %prec APP                          {% annM $ mkApp $1 $2   }
    | 'if' Expr 'then' Expr 'else' Expr            {% annM $ mkIf $2 $4 $6 }
    | 'fix' Expr                                   {% annM $ mkFix $2      }
    | Expr BUILTIN                                 {% annM $ \loc -> mkApp (mkBuiltin $2 loc) $1 loc }
    | BUILTIN                                      {% annM $ mkBuiltin $1 }
    | '(' Expr ')'                                 { $2 }
    | Constructor                                  { $1 }

Constructor
    : Tuple                                        { $1 }
    | Record                                       { $1 }
    | List                                         { $1 }
    | INT                                          {% annM $ mkInt $1 }
    | 'true'                                       {% annM $ mkBool True  }
    | 'false'                                      {% annM $ mkBool False }
    | STRING                                       {% annM $ mkString $1  }
    | ATOM                                         {% annM $ mkAtom $1 }

Tuple
    : '(' Expr ',' tuple_tail ')'                  {% annM $ mkTuple $2 $4 }

tuple_tail
    : Expr                                         { $1 :| [] }
    | Expr ',' tuple_tail                          { $1 <| $3 }

Record
    : '{' RecordLabelExprList '}'                  {% annM $ \loc -> mkExtendRecord $2 (mkEmptyRecord loc) loc }
    | '{' '}'                                      {% annM $ mkEmptyRecord        }
    | '{' RecordLabelExprList '|' Expr '}'         {% annM $ mkExtendRecord $2 $4 }
    | Expr '.' IDENT                               {% annM $ mkSelectRecord $3 $1 }

RecordLabelExprList
    : IDENT '=' Expr                               { ($1, $3) :| [] }
    | RecordLabelExprList ',' IDENT '=' Expr       { ($3, $5) <| $1 }

Arg
    : '{' DeconstructPattern '}'                   { DeconstructArg $2 }
    | IDENT                                        { VanillaArg $1     }

ArgList
    : Arg                                          { $1 :| [] }
    | Arg ArgList                                  { $1 <| $2 }

DeconstructPattern
    : IDENT                                        { $1 :| []  }
    | DeconstructPattern ',' IDENT                 { $3 <| $1  }

ident_comma_list
    : IDENT                                        { $1 :| [] }
    | ident_comma_list ',' IDENT                   { $3 <| $1 }

List
    : '[' list_content ']'                         {% annM $ mkList $2 }

list_content
    : Expr                                         { [$1]       }
    | Expr ',' list_content                        { [$1] ++ $3 }
    |                                              { []         }

Type
    : 'Int'                                        { TInt         }
    | 'String'                                     { TString      }
    | 'Bool'                                       { TBool        }
    | ATOM                                         { TAtom $1     }
    | Type '->' Type                               { TArrow $1 $3 }
    | '(' Type ')'                                 { $2           }
    | '[' Type ']'                                 { (TList $2)   }
    | '(' Type ',' TupleType ')'                   { TTuple $2 $4 }
    | '{' RowType '}'                              { TRecord $ TExtendRow (Map.fromList $2) TEmptyRow }

RowType
    : IDENT '::' Type                              { [($1, $3 :| [])] }
    | IDENT '::' Type ',' RowType                  { [($1, $3 :| [])] ++ $5 }

TupleType
    : Type                                         { $1 :| [] }
    | Type ',' TupleType                           { $1 <| $3 }

{

infixr 9 |>

(|>) :: NonEmpty a -> a -> NonEmpty a
t |> v = L.reverse $ v <| L.reverse t

annM :: (SourceLocation -> a) -> Alex a
annM fn = alexGetPosition >>= go
    where go (AlexPn _ line column) = return $ fn (SourceLocation line column)

thenP :: Alex a -> (a -> Alex b) -> Alex b
thenP = (>>=)

returnP :: a -> Alex a
returnP = return

happyError :: Alex a
happyError = alexGetPosition >>= \(AlexPn _ line col) -> alexShowError (line, col, Nothing)

data ErrClass
    = Syntactical (Maybe String)
    | Lexical
    | Message String
    deriving (Show, Eq)

data ParseError = ParseError
    { errLine  :: Int
    , errPos   :: Int
    , errClass :: ErrClass
    } deriving (Show, Eq)

instance Error ParseError

instance Pretty ErrClass where
    pretty e = pretty . show $ e

instance Pretty ParseError where
    pretty ParseError { errLine, errPos, errClass } =
        "error" <+> pretty errClass <+>
        "on line" <+> pretty errLine <+>
        "on position" <+> pretty errPos

showErrPrefix = "show-error: " :: String
lexicalErrorPrefix = "lexical error at line " :: String

parse :: String -> Either ParseError Module
parse s = case runAlex s happyParser of
    Right x -> Right x
    Left str | showErrPrefix `isPrefixOf` str ->
                 let (line, column, m) = (read (drop (length showErrPrefix) str) :: (Int, Int, Maybe String))
                 in Left (ParseError line column (Syntactical m))
             | lexicalErrorPrefix `isPrefixOf` str ->
                  let info = drop (length lexicalErrorPrefix) str
                      lineStr = takeWhile (/= ',') info
                      columnStr = drop (9 + length lineStr) info
                   in Left (ParseError (read lineStr) (read columnStr) Lexical)
             | otherwise  -> Left (ParseError 0 0 (Message str))

}
