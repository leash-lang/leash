{-# LANGUAGE OverloadedLists #-}
module Language.Leash.Inference
  ( typecheck
  , InferenceError
  ) where

import           Control.Monad                  ( unless
                                                , zipWithM
                                                )
import           Data.Functor                   ( (<&>) )
import           Data.Functor.Foldable          ( cata )
import qualified Data.List.NonEmpty            as L
import           Data.List.NonEmpty             ( (<|)
                                                , NonEmpty((:|))
                                                )
import qualified Data.Map                      as Map
import           Data.Map                       ( Map )
import qualified Data.Set                      as Set
import           Data.Text                      ( pack )
-- import           Data.Tuple.Extra               ( fst3
--                                                 , snd3
--                                                 , thd3
--                                                 )
import           Language.Leash.Annotated
import           Language.Leash.Ast
import           Language.Leash.SourceLocation  ( SourceLocation(..) )
import           Language.Leash.Type
import           Prettyprinter           hiding ( annotate )

import           Control.Monad.Except           ( Except
                                                , MonadError
                                                , runExcept
                                                , throwError
                                                )
import           Control.Monad.State            ( MonadState
                                                , StateT
                                                , evalStateT
                                                , get
                                                , gets
                                                , modify
                                                , put
                                                , withStateT
                                                )
--------------------------------------------------------------------------------

newtype Inference a = Inference { _runInference :: StateT InferenceState (Except InferenceError) a }
  deriving (Applicative, Functor, Monad, MonadState InferenceState, MonadError InferenceError)

runInference :: Inference a -> InferenceState -> Either InferenceError a
runInference m st = runExcept $ evalStateT (_runInference m) st

--------------------------------------------------------------------------------

data InferenceState = InferenceState
  { count       :: Int
  , srcLocation :: SourceLocation
  , context     :: Context
  }

initialState :: Context -> InferenceState
initialState context =
  InferenceState { count = 0, srcLocation = SourceLocation 0 0, context }

mapContext :: (Context -> Context) -> InferenceState -> InferenceState
mapContext f s = s { context = f $ context s }

setSrcLocation :: SourceLocation -> InferenceState -> InferenceState
setSrcLocation src s = s { srcLocation = src }

withState :: (InferenceState -> InferenceState) -> Inference a -> Inference a
withState f (Inference m) = Inference $ withStateT f m

withContext :: (Context -> Context) -> Inference a -> Inference a
withContext = withState . mapContext

lookupType :: Text -> Inference (Maybe Sigma)
lookupType t = gets (Map.lookup t . context)

updateLoc :: SourceLocation -> Inference ()
updateLoc loc = modify $ setSrcLocation loc

--------------------------------------------------------------------------------

data InferenceError
  = NotInScopeError !Text !SourceLocation
  | UnifyError !Tau !Tau !SourceLocation
  | OccursError !Text !Tau !SourceLocation
  | DefaultNotLastError !SourceLocation
  deriving (Eq)

notInScopeError :: Text -> Inference a
notInScopeError x = gets srcLocation >>= throwError . NotInScopeError x

unifyError :: Tau -> Tau -> Inference a
unifyError t0 t1 = gets srcLocation >>= throwError . UnifyError t0 t1

occursError :: Text -> Tau -> Inference a
occursError x t = gets srcLocation >>= throwError . OccursError x t

--------------------------------------------------------------------------------

unify :: Tau -> Tau -> Inference Theta

unify (TVar x)     t            = bind x t

unify t            (TVar x)     = bind x t

unify TInt         TInt         = pure Map.empty

unify TBool        TBool        = pure Map.empty

unify TString      TString      = pure Map.empty

unify (TArrow f x) (TArrow g y) = do
  s0 <- unify f g
  s1 <- unify (apply s0 x) (apply s0 y)
  return (s1 <@> s0)

unify (TList   a ) (TList   b )           = unify a b

unify (TRecord r0) (TRecord r1)           = unify r0 r1

unify (TAtom a) (TAtom b) | a == b        = pure Map.empty

unify t1@(TTuple t ts) t2@(TTuple t' ts') = do
  unless (L.length ts == L.length ts') $ unifyError t1 t2
  s <- sequence $ uncurry unify <$> ((t <| ts) `L.zip` (t' <| ts'))
  return $ foldr (<@>) Map.empty s

unify TEmptyRow             TEmptyRow             = pure Map.empty

unify t0@(TExtendRow l0 r0) t1@(TExtendRow l1 r1) = do
  let (labels0, rest0) = flattenRow l0 r0
      (labels1, rest1) = flattenRow l1 r1
      (uniq0  , uniq1) = bothWays Map.difference labels0 labels1
  same <- sequence $ Map.intersectionWith (over2 L.head unify) labels0 labels1
  diff <- case (Map.null uniq1, Map.null uniq0) of
    (True , True ) -> return Map.empty
    (True , False) -> unify rest1 $ TExtendRow uniq0 rest0
    (False, True ) -> unify rest0 $ TExtendRow uniq1 rest1
    (False, False) -> fresh >>= \mt -> case rest0 of
      TEmptyRow -> unify rest0 $ TExtendRow uniq1 mt
      TVar _    -> unify rest1 $ TExtendRow uniq0 mt
      _         -> unifyError t0 t1
  return (foldr (<@>) Map.empty same <@> diff)

unify t0 t1 = unifyError t0 t1

  --------------------------------------------------------------------------------

flattenRow
  :: Map Text (L.NonEmpty Tau) -> Tau -> (Map Text (L.NonEmpty Tau), Tau)
flattenRow labels rest = case rest of
  (TExtendRow labels' r) -> flattenRow (Map.unionWith (<>) labels labels') r
  TEmptyRow              -> (labels, TEmptyRow)
  x                      -> (labels, x)

  --------------------------------------------------------------------------------


bind :: Text -> Tau -> Inference Theta

bind x (TVar y) | x == y    = return Map.empty
                | otherwise = return $ Map.singleton x (TVar y)

bind x t | x `Set.member` tyVars t = occursError x t
         | otherwise               = return $ Map.singleton x t

--------------------------------------------------------------------------------


over2 :: (b -> a) -> (a -> a -> c) -> b -> b -> c
over2 x y z1 z2 = y (x z1) (x z2)

bothWays :: (a -> a -> b) -> a -> a -> (b, b)
bothWays f a0 a1 = (f a0 a1, f a1 a0)
--------------------------------------------------------------------------------

fresh :: Inference Tau
fresh = do
  st <- get
  put st { count = count st + 1 }
  return . TVar $ "$" <> (pack . show . count $ st)

inst :: Sigma -> Inference Tau
inst (TMono t     ) = return t
inst (TForAll xs t) = do
  tvars <- mapM (const fresh) xs
  let substitutions = Map.fromList $ zip xs tvars
  return $ apply substitutions t


requireType :: Text -> Inference Sigma
requireType x = lookupType x >>= \case
  Nothing -> notInScopeError x
  Just t  -> return t

--------------------------------------------------------------------------------

gen :: Tau -> Inference Sigma
gen t = gets context <&> flip generalize t

--------------------------------------------------------------------------------

infer :: Ast' -> Inference (Context, Tau)
infer term =
  cata alg term
    >>= (\tau -> do
          ctx <- gets context
          return (ctx, tau)
        )
    .   snd
 where

  alg (TypedF x t :*: K loc) = do
    updateLoc loc
    (s0, t0) <- x
    s1       <- unify t0 t
    return (s1 <@> s0, t)

  alg (VarF x :*: K loc) = do
    updateLoc loc
    pt <- requireType x
    mt <- inst pt
    return (Map.empty, mt)

  alg (AppF f x :*: K loc) = do
    updateLoc loc
    (s0, t0) <- f
    (s1, t1) <- withContext (apply s0) x
    mt       <- fresh
    s2       <- unify (apply s1 t0) (t1 `TArrow` mt)
    return (s2 <@> s1 <@> s0, apply s2 mt)

  alg (AbsF x e :*: K loc) = do
    updateLoc loc
    mt       <- fresh
    (s0, t0) <- withContext (Map.insert x $ TMono mt) e
    return (s0, apply s0 $ mt `TArrow` t0)

  alg (LetF x e0 e1 :*: K loc) = do
    updateLoc loc
    (s0, t0) <- e0
    pt       <- withContext (apply s0) (gen t0)
    (s1, t1) <- withContext (apply s0 . Map.insert x pt) e1
    return (s1 <@> s0, t1)

  alg (BuiltinF builtin :*: K loc) = do
    updateLoc loc
    mt <- requireType builtin >>= inst
    return (Map.empty, mt)

  alg (IntF _ :*: K loc) = do
    updateLoc loc
    return (Map.empty, TInt)

  alg (BoolF _ :*: K loc) = do
    updateLoc loc
    return (Map.empty, TBool)

  alg (StringF _ :*: K loc) = do
    updateLoc loc
    return (Map.empty, TString)

  alg (IfF e0 e1 e2 :*: K loc) = do
    updateLoc loc
    (s0, t0) <- e0
    (s1, t1) <- e1
    (s2, t2) <- e2
    s3       <- unify t0 TBool
    s4       <- unify t1 t2
    return (s4 <@> s3 <@> s2 <@> s1 <@> s0, t1)

  -- | TODO: could this be a prim op?
  alg (FixF e :*: K loc) = do
    updateLoc loc
    (s0, t0) <- e
    mt       <- fresh
    s1       <- unify (mt `TArrow` mt) t0
    let rt = apply (s1 <@> s0) mt
    return (s1 <@> s0, rt)

  alg (EmptyRecordF :*: K loc) = do
    updateLoc loc
    return (Map.empty, TRecord TEmptyRow)

  alg (ExtendRecordF xs r :*: K loc) = do
    updateLoc loc
    (s0, tr) <- r
    labels   <- mapM sequence xs
    mt       <- fresh
    s1       <- unify (TRecord mt) tr
    let row = fmap (fmap snd) labels
    return (s1 <@> s0, apply (s1 <@> s0) $ TRecord $ TExtendRow row mt)

  alg (SelectRecordF label record :*: K loc) = do
    updateLoc loc
    (s0, tr) <- record
    restt    <- fresh
    fieldt   <- fresh
    let t = TRecord $ TExtendRow (Map.singleton label $ fieldt :| []) restt
    s1 <- unify t tr
    let rt = apply (s1 <@> s0) fieldt
    return (s1 <@> s0, rt)

  alg (AtomF x :*: K loc) = do
    updateLoc loc
    t <- inst (TMono (TAtom x))
    return (Map.empty, t)

  alg (TupleF x xs :*: K loc) = do
    updateLoc loc
    (s0, t0) <- x
    (s1, t1) <- sequence xs <&> L.unzip
    return (foldr (<@>) s0 s1, TTuple t0 t1)

  alg (ListF xs :*: K loc) = do
    updateLoc loc
    mt       <- fresh
    (ss, ts) <- sequence xs <&> unzip
    s        <- case ts of
      []  -> return Map.empty
      ts' -> do
        ss' <- mapAdjacentM unify (mt : ts')
        return (foldr (<@>) Map.empty $ ss ++ ss')
    return (s, TList $ apply s mt)

mapAdjacentM :: Monad m => (a -> a -> m b) -> [a] -> m [b]
mapAdjacentM f xs = zipWithM f xs (Prelude.tail xs)

--------------------------------------------------------------------------------

typecheck :: Context -> Ast' -> Either InferenceError (Context, Tau)
typecheck ctx term = runInference (infer term) (initialState ctx)

--------------------------------------------------------------------------------

instance Pretty InferenceError where
  pretty (NotInScopeError x loc) = "not in scope: " <> pretty x <+> pretty loc
  pretty (UnifyError t0 t1 _   ) = nest 4 $ vsep
    [ "failed to unify"
    , pretty t0
    , pretty t1
    , pretty $ show t0
    , pretty $ show t1
    ]
  pretty (OccursError x t _) =
    nest 4 $ vsep ["occurs check", pretty x, pretty t]
  pretty (DefaultNotLastError _) =
    "the default case must be last in the case list"
