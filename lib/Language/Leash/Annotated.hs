module Language.Leash.Annotated
  ( Annotated
  , K(..)
  , (:*:)(..)
  , (:+:)(..)
  , annotate
  , stripAnn
  , getAnn
  , withAnn
  , mapAnn
  ) where

import Data.Data
import Data.Fix (Fix(..))
import Data.Functor.Foldable (cata)

newtype K a b = K { unK :: a }
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable, Data)

data (f :*: g) a = (:*:) { left :: f a, right :: g a }
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable, Data)

data (f :+: g) a = InL (f a) | InR (g a)
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable, Data)

type family Annotated a f
type instance Annotated a (Fix f) = Fix (f :*: K a)

annotate :: Functor f => a -> Fix f -> Annotated a (Fix f)
annotate ann = cata alg where alg e = Fix (e :*: K ann)

stripAnn :: Functor f => Annotated a (Fix f) -> Fix f
stripAnn = cata alg where alg (e :*: _ ) = Fix e

getAnn :: Functor f => Annotated a (Fix f) -> a
getAnn = unK . right . unFix

withAnn :: Functor f => a -> f (Annotated a (Fix f)) -> Annotated a (Fix f)
withAnn ann e = Fix (e :*: K ann)

mapAnn :: Functor f => (a -> a) -> Annotated a (Fix f) -> Annotated a (Fix f)
mapAnn fn obj = annotate (fn $ getAnn obj) (stripAnn obj)
