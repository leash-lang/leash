module Data.Either.Extra
  ( fromRight
  ) where

fromRight :: Either a b -> b
fromRight (Right a) = a
fromRight _         = error "fromRight: Left"
