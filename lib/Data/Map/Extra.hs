module Data.Map.Extra
  ( deleteList
  , unionList
  ) where

import           Data.Map                       ( Map
                                                , delete
                                                , fromList
                                                , union
                                                )

deleteList :: Ord k => [k] -> Map k a -> Map k a
deleteList xs m = foldr delete m xs

unionList :: Ord k => [(k, a)] -> Map k a -> Map k a
unionList = union . fromList
