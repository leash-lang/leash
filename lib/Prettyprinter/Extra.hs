module Prettyprinter.Extra
  ( braced
  , label
  ) where

import qualified Data.Text                     as T
import           Prettyprinter

label :: Pretty b => T.Text -> b -> Doc a
label l v = pretty l <+> "=" <+> pretty v

braced :: [Doc a] -> Doc a
braced = group . encloseSep (flatAlt "{ " "{") (flatAlt " }" "}") ", "
