{
  description = "leash, the purely functional shell";

  inputs = {
    continuous-integration.url = "git+https://gitlab.com/leash-lang/continuous-integration.git";
    nixpkgs.follows = "continuous-integration/nixpkgs";
    flake-utils.follows = "continuous-integration/flake-utils";
    pre-commit-hooks.follows = "continuous-integration/pre-commit-hooks";
    bats.url = "git+https://gitlab.com/leash-lang/bats.nix.git";
    bats.inputs.nixpkgs.follows = "continuous-integration/nixpkgs";
    bats.inputs.flake-utils.follows = "continuous-integration/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, bats, pre-commit-hooks, continuous-integration, ... }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        inherit (nixpkgs.lib) flip;

        haskellPackages = continuous-integration.haskellPackages.${system};
        pkgs = nixpkgs.legacyPackages.${system};

        # This is due to a GHC bug, see https://gitlab.haskell.org/ghc/ghc/-/issues/18320
        overrideCabal = (flip pkgs.haskell.lib.overrideCabal) { enableLibraryProfiling = false; };

        # Due to hls bug, see https://github.com/haskell/haskell-language-server/issues/2398
        haskell-language-server = (import (
          pkgs.fetchFromGitHub {
            owner = "haskell";
            repo = "haskell-language-server";
            rev = "745ef26f406dbdd5e4a538585f8519af9f1ccb09";
            sha256 = "sha256-Reo9BQ12O+OX7tuRfaDPZPBpJW4jnxZetm63BxYncoM=";
          }
        )).defaultPackage.${system};
      in
      rec {
        packages = rec {
          leash = overrideCabal (haskellPackages.callCabal2nix "leash" self { });
          defaultPackage = packages.leash;
        };

        checks.pre-commit-check = pre-commit-hooks.lib.${system}.run {
          inherit (packages.defaultPackage) src;
          hooks.nixpkgs-fmt.enable = true;
          hooks.brittany.enable = true;
          hooks.hlint.enable = true;
          hooks.shellcheck.enable = true;
          hooks.yamllint.enable = true;

          # Due to a bug in brittany, see https://github.com/lspitzner/brittany/issues/340
          hooks.brittany.excludes = [ "lib/Language/Leash/Annotated.hs" ];
        };

        devShell = packages.leash.env.overrideAttrs (orig: {
          nativeBuildInputs = with pkgs; [
            cabal-install
            haskellPackages.apply-refact
            haskellPackages.weeder
            haskellPackages.pointfree
            haskellPackages.hoogle
            haskellPackages.stan
            haskellPackages.brittany
            taskell
            bats
          ] ++ [
            haskell-language-server
          ];
        });
      });
}
