load "common.bash"

@test "abstraction with a flat record pattern unifies with the body" {
    run typecheck "fn { x, y } => x + y"
    assert_output "forall a. {y :: Int | x :: Int | a} -> Int"
    assert_success
}

@test "abstraction with a flat record pattern unifies with a record with extra fields" {
    run typecheck "(fn { x, y } => { x = x, y = y }) { x = 1, y = 2, z = 3 }"
    assert_output "{x :: Int, y :: Int | {}}"
    assert_success
}

@test "abstraction with a flat record pattern can evaluate a record with extra fields" {
    run evaluate "(fn { x, y } => { x = x, y = y }) { x = 1, y = 2, z = 3 }"
    assert_output "{x = 1, y = 2}"
    assert_success
}

@test "pattern matching grabs the correct field (abs version)" {
    run evaluate "(fn { x, y } => x) { x = 1, y = 2 }"
    assert_output "1"
    assert_success
}

@test "pattern matching grabs the correct field (abs version swapped)" {
    run evaluate "(fn { x, y } => y) { x = 1, y = 2 }"
    assert_output "2"
    assert_success
}

@test "pattern matching grabs the correct field (let version)" {
    run evaluate "let f { x, y } = x in f { x = 1, y = 2 }"
    assert_output "1"
    assert_success
}

@test "pattern matching grabs the correct field (let version swapped)" {
    run evaluate "let f { x, y } = y in f { x = 1, y = 2 }"
    assert_output "2"
    assert_success
}

@test "pattern matching in let expression" {
    run evaluate "let { a, b } = { a = 1, b = 2 } in a"
    assert_output "1"
    assert_success
}

@test "pattern matching in let expression again" {
    run evaluate "let { a, b } = { a = 1, b = 2 } in a + b"
    assert_output "3"
    assert_success
}
