load "common.bash"

@test "string concatenation" {
    run evaluate '
        "hello" <> ", " <> "world" <> "!"
    '
    assert_output '"hello, world!"'
    assert_success
}
