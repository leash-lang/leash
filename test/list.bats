load "common.bash"

@test "an empty list typechecks" {
    run typecheck "[]"
    assert_output "forall a. [a]"
    assert_success
}

@test "a list of ints typechecks" {
    run typecheck "[1,2]"
    assert_output "[Int]"
    assert_success
}

@test "an empty list evaluates" {
    run evaluate "[]"
    assert_output "[]"
    assert_success
}

@test "a list of ints evaluates" {
    run evaluate "[1,2]"
    assert_output "[1, 2]"
    assert_success
}

@test "a list of strings typechecks" {
    run typecheck '["a", "b"]'
    assert_output "[Str]"
    assert_success
}

@test "a list of strings evaluates" {
    run evaluate '["a","b"]'
    assert_output '["a", "b"]'
    assert_success
}

@test "two lists concatenates" {
    run evaluate '[1] ++ [2]'
    assert_output '[1, 2]'
    assert_success
}

@test "two empty lists concatenates" {
    run evaluate '[] ++ []'
    assert_output '[]'
    assert_success
}

@test "a list and and empty list concatenates" {
    run evaluate '[] ++ [1]'
    assert_output '[1]'
    assert_success
}

@test "three lists concatenates" {
    run evaluate '[1] ++ [2] ++ [3]'
    assert_output '[1, 2, 3]'
    assert_success
}
