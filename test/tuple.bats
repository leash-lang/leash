load "common.bash"

@test "a basic tuple typechecks" {
    run typecheck "(1, 2, true)"
    assert_output "(Int, Int, Bool)"
    assert_success
}

@test "a basic tuple evaluates" {
    run evaluate "(1, 2, true)"
    assert_output "(1, 2, true)"
    assert_success
}

@test "a nested tuple typechecks" {
    run typecheck "(1, (true, false), true)"
    assert_output "(Int, (Bool, Bool), Bool)"
    assert_success
}

@test "a nested tuple evaluates" {
    run evaluate "(1, (true, false), true)"
    assert_output "(1, (true, false), true)"
    assert_success
}
