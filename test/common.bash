# shellcheck disable=SC1091
source error.bash
source lang.bash
source output.bash
source assert.bash

leash() {
    local -r out="${TMPDIR:-/tmp}/leash"
    if ! [ -L "$out" ]; then
        nix build --out-link "$out" "$(git rev-parse --show-toplevel)" &>/dev/null
    fi
    "$out/bin/leash" "$@"
}

typecheck() {
    leash --debug --typecheck /dev/stdin <<< "$1"
}

evaluate() {
    leash --debug <<< "$1"
}
