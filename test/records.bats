load "common.bash"

@test "a basic record typechecks" {
    run typecheck "{ x = 12, y = 13 }"
    assert_output "{x :: Int, y :: Int | {}}"
    assert_success
}

@test "a basic record evaluates" {
    run evaluate "let r = { x = 12, y = 13 } in r.x + r.y"
    assert_output "25"
    assert_success
}

@test "a nested record typechecks" {
    run typecheck "{ x = 1, y = { z = 2 } }"
    assert_output "{x :: Int, y :: {z :: Int | {}} | {}}"
    assert_success
}

@test "a nested record evaluates" {
    run evaluate "let r = { x = 1, y = { z = 2 } } in r.x + r.y.z"
    assert_output "3"
    assert_success
}
