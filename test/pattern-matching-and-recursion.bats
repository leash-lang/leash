load "common.bash"

@test "factorial using a record for argument" {
    run evaluate "
        let f { n } =
            if n == 0 then 1
            else n * f { n = n - 1 }
        in

        f { n = 5 }
    "
    assert_output "120"
    assert_success
}
