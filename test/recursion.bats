load "common.bash"

@test "fix recursion" {
    run evaluate "
        let fact n =
            if n == 0 then 1
            else n * fact (n-1)
        in

        fact 5
    "
    assert_output "120"
    assert_success
}
