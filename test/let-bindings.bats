load "common.bash"

@test "basic let binding typechecks" {
    run typecheck "let x = 12 in 12"
    assert_output "Int"
    assert_success
}

@test "basic let binding evaluates" {
    run evaluate "let x = 13 in x"
    assert_output "13"
    assert_success
}
