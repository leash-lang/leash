load "common.bash"

@test "case of two variants" {
    run evaluate "
        let f x = case x of
            :event1 val -> val + 1;
            :event2 val -> val + 2;

        in { one = f :event1 5
           , two = f :event2 5 }
    "
    assert_output "{one = 6, two = 7}"
    assert_success
}
