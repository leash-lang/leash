module Data.Map.ExtraSpec
  ( spec
  ) where

import           Data.Map
import           Data.Map.Extra
import           Data.Text                      ( Text )
import           Prelude                 hiding ( null )
import           Test.Hspec

spec :: Spec
spec = do
  describe "deleteList" $ do
    it "should remove a single key" $ do
      let m = singleton "x" 1
          e = empty :: Map Text Int
      deleteList ["x"] m `shouldBe` e
    it "should not touch keys not listed" $ do
      let m = fromList [("x", 1), ("y", 2)]
          r = singleton "y" 2 :: Map Text Int
      deleteList ["x"] m `shouldBe` r
