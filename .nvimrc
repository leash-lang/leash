lua<<EOF
package.path = vim.fn.getcwd() .. '/lsp-server/?.lua;' .. package.path
require 'leash_lsp_server'

require'lspconfig'.leash_lsp_server.setup{}
require'lspconfig'.hls.setup{}
EOF
