# Lazy Effects Shell

Leash is a next generation shell with static typing and constrained effects that allows one to write shell scripts that
are secure and type safe. For you who are familiar with functional programming language, leash uses bidirectional
typechecking for higher rank polymorphism. For the mere mortals, this simply means leash is statically typed like rust
or C++ but it is much better at deducing types so you don't actually have to type them out yourself.

## Links

- https://www.lesswrong.com/posts/vTS8K4NBSi9iyCrPo/a-reckless-introduction-to-hindley-milner-type-inference
- https://github.com/kritzcreek/fby19
- https://github.com/willtim/Expresso
- https://medium.com/@dhruvrajvanshi/type-inference-for-beginners-part-1-3e0a5be98a4b

## What is leash all about?

Leash isn't trying to be a general purpose language, it's a domain specific language for processing text and managing
processes, just like bash, fish, or zsh. However, shell scripts are generally very hard to get right because of a few
reasons.

- Shells are generally untyped, everything is a string, the sanity of the script can't be be verified prior to running
  it. In leash everything has a type, and the type inference algorithm will verify that all the types align before
  executing any code.

- Commandline argument parsing is a pain, which is a bit weird since most shell scripts will use them. In leash
  commandline arguments are a first class citizen of the language and very easy to get right.

- Shells are imperative, meaning you  write some control flow with loops, if/else statements, which you intermingle with
  effects like writing files and reading user input. In leash we program our scripts in a pure functional manner which
  has a declarative nature, you declare the effects your program will have upfront and then use functions to combine
  them.

If you're not used to functional programming you shouldn't worry. As stated earlier leash is a domain specific language
for processing tex and managing processes, and the lack of ambitions on claiming world domination for a language to rule
them all we can re-use common idioms and concepts like argument parsing and effect chaining through syntactic sugar
built in to the language. If you come from a functional programming backround such as Haskell or ML you will quickly
realize that leash lacks a lot of the more advanced features like typeclasses and meta template programming, but this is
intentional and is one of the core values of leash that it's simple, idiomatic, and easy to understand.

## A very basic example

The simplest example of them all is one where we just run a command like `ls`.

```
let main { } = $ls
```

The above script takes no arguments as it's only main function takes an empty record as its argument. The main function
returns an effect that runs the `ls` function without any arguments.  To create an effect you use the effect operator
`$`, which creates an effect which you can think of like a recipe to run some effectful code like `ls`.

To demonstrate how the commandline argument parsing of leash works we can take the regular `ls` command and create a
wrapper script `lsh` that changes things around a little. If you do `ls -h` in your terminal you'll see all the
arguments you can pass to `ls`. You can see that it has a flag `-h` for printing the help message, and a flag `-l` for
using a long list format. If you pass it both the `-h` and `-l` flags what would you expect to happen? Well, what does
happen is that `ls` by convention prints the help text whenever you pass it the `-h` flag, then it ignores the other
flags. Relying on conventions like this can be a problem since it makes the commandline argument specifications of
programs imprecise and inconsistent. So our own `lsh` program written in leash alleviates this problem by encoding in
the type system that you either pass it the `-h` flag or a combination of the `-l` and `-a` flags.

NOTE: The help flag is a bad example, we should find a better one.

```
let fromMaybe (Just a) = [a]
    fromMaybe Nothing = []
    fromRecord args =
      let go acc { } = acc
          go acc { a, ...rest } = go (acc ++ fromMaybe a) rest
          go acc { l, ...rest } = go (acc ++ fromMaybe l) rest
      in go [] args
    main { h } = $ls (fromMaybe  h)
    main r     = $ls (fromRecord r)
in main
```

Again, coming from a non functional background this might look arcane, so let me explain. The interpreter will take all
the arguments you pass the script and create a record out of them, where every flag corresponds to a `Maybe String` type
wrapping the actual string value of the flag, i.e. `"-a"`, `"-l"`, or `"-h"` respectively. When the program is passed
to the typechecker it'll actually look like this.

```
let expr =
  let fromMaybe (Just a) = [a]
      fromMaybe Nothing = []
      fromRecord args =
        let go acc { } = acc
            go acc { a, ...rest } = go (acc ++ fromMaybe a) rest
            go acc { l, ...rest } = go (acc ++ fromMaybe l) rest
        in go [] args
      main { h } = $ls (fromMaybe  h)
      main r     = $ls (fromRecord r)
  in main
in expr { a = Just "-a", l = Just "-l" }
```

Observe how the only thing that can actually fail apart from passing nonsensical arguments is the `$ls` call since it's
the only non-deterministic part of the program. That's pretty neat!

<details>
    <summary markdown="span">Drafts of the next steps.</summary>

    ## Effects

    Since leash is purely functional, as in functions are functions in a mathematical sense and take values as
    parameters, make a computation, and return a value. They cannot do anything "effectful" like writing to disk or
    launching the missiles. If you're thinking something like "well that sucks, my shell scripts actually have to do
    stuff" then don't stop reading, we'll get to that.

    In leash an effect is just a type that is kind of like a recipe for what effects to perform, a script if you will.
    This script is lazily evaluated whenever you want to request its value. This has the nice property that you can
    program by the "imperative shell, functional core" idiom. Or in other words, you declare all the effects your
    program can perform up front and combine those effects together with your business logic that can't have any
    effects. This makes a really clean abstraction.
</details>
